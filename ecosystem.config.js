module.exports = {
    apps: [
      {
        name: 'Fotodd',
        exec_mode: 'cluster',
        instances: 'max',
        script: './.output/server/index.mjs'
      }
    ]
  }