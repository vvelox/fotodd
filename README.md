# Read Me

live on an extren Server: http://89.58.42.151/

## Auswahl der technischen Software (dt)

### Nuxt, Webpack, Vue.js
Als Hauptframeworks nutze ich hier die Kombi aus [Nuxt v3](https://nuxtjs.org/) (Backend), [Webpack](https://webpack.js.org/) (Middelware) und [Vue.js 3](https://vuejs.org/) (Frontend).<br/>
Doch warum diese Kombination? Ich wollte in diesem Projekt nuxt v3 ausprobieren. Auf Arbeit nutzten wir hauptsaechlich nuxt v2, weil es die aktuelle stable version ist und wie wollte gucken wie es sich mit dem Nachfolger so arbeitet. (Fazit: An vielen Stellen fehlt noch Doku und man muss viel basteln)<br/>
Die Kombination aus Nuxt, Webpack und Vue ist aber keines Wegs experimentiert, sodern eigentlich so standart. Nuxt ist direkt fuer Vue.js entwickelt und Webpack ist so die Middelware auf dem Markt, da gibt es kaum Konkurenz.

### Bootstrap, Bootstrap-Icons
Als Component-Libray nuzte ich Bootstrap, da Bootstrap sehr viel mitbringt, aber auch sehr anpassbar ist. Die vielen Helper-Classen von Bootstrap erleichern das Gestallten der Webseite enorm. Ich moechte eine Row haben, also packe ich die Klasse `row` dran, ich moechte dadrinnen einen Abstand haben, also packe ich ein `g-5` dahintern. Der Text soll weiss sein? Dann packe ich ein `text-white` dahinter. Und das alles ohne die css klasse zu schreiben.<br/>
Ebenso sind die Icons aus Bootstrap-Icons sehr schoen und einfach zu implenentieren.

### Mapbox
In der Kurs-Detail-Seite ist eine Karte als i-Frame eingebunden, welche die Api von [Mapbox](https://www.mapbox.com/) nutzt. Mapbox ist einer der groessten Player in anpassbaren Kartendaten. Es nutzt die Kartendaten von [OSM](https://www.osm.org) und bietet die moeglichkeit jeden Layer farblich anzupassen. Also Fluesse, Strassen, Felder, Berge, etc. Sachen einzublenden oder auszublenden. Also die eigene Karte so anzupassen wie es zur eigenen Brand oder Seite gerade passt.

## Beschreibung des Vereines (dt)
Die Webseite dreht sich um einen fiktiven Verein, dem: Foto Verein Dresden Sued e.V. Der Verein versucht fotografiebegeisterte Menschen zusammen zubringen und bietet fuer jeden bestimmte Kurse an (hier ist das interaktive Kontaktformular - Termin buchen :)) Ebenso schreibt es einmal im Monat einen Bericht auf ihre Webseite und in Ihr Magazin. Das Magazin welches alle Mitglieder bekommen, kommt monatlich raus und ist ein Pardon zu der Webseite. Deswegen ist es auf der Index Seite auch sofort verlinkt.

## Setup on your own

Make sure you have installed node.js to install the dependencies like:

```bash
# install pnpm
npm install pnpm #only once

# pnpm (recommend and tested)
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000 with:

```bash
pnpm dev
```

## Production

Build the application for production:

```bash
pnpm build
```

start pm2:

```bash
pnpm pm2 start
```

listen on http://localhost:3000
to route outside the server please setup nginx or an alternative reverse proxy

## Eigenanteil (dt)

Fremdquellcode sind zum Teil manche cofig files und alles was in der package.json steht. Bilder sind selbst fotografiert und bearbeitet, Design ist von mir im Rahmen der Module Medesign an der THB entstanden, Icons wind in der Package.json zu sehen bootstrap-icon, ausser das Logo.
