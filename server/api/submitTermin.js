import * as url from "url"
export default defineEventHandler((event) => {
    const params = url.parse(event.req.url, true).query
    const res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(res.test(String(params.email).toLowerCase())) {
      console.log('Neue Anmeldung --- Kurs: ' + params.kurs + ' E-Mail: ' + params.email + ' Termin: ' + params.termin)
      return {
        valied: true
      }
    } else {
      return {
        valied: false
      }
    }
  })
  